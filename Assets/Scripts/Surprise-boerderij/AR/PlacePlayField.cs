﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR;
using UnityEngine.XR.ARSubsystems;
using System;

//nino
 
/// <summary>
/// this scipt works together with the arsession origin and the ar pose to place an object on real objects
/// because of this pose it takes in consideration what direction and rotation the phone has when it puts down the object
/// </summary>

public class PlacePlayField : MonoBehaviour
{
    [SerializeField]
    private GameObject _objectToPlace;
    [SerializeField]
    private GameObject _placementIndicator;
    private Camera _myCamera;
    private ARSessionOrigin _arOrigin;
    private ARRaycastManager _raycastManager;
    private Pose _placementPose;
    private bool _placementPoseIsValid = false;
    

    void Start()
    {
        _arOrigin = FindObjectOfType<ARSessionOrigin>();
        _raycastManager = FindObjectOfType<ARRaycastManager>();
    
    }

    void Update()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();

        if (_placementPoseIsValid &&  Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            PlaceObject();
        }
    }

    private void PlaceObject()    //places the base prefab
    {
        Instantiate(_objectToPlace, _placementPose.position, _placementPose.rotation);
        
        gameObject.SetActive(false);
    }

    private void UpdatePlacementIndicator() // updates placement indicator base on the placement pose.
    {
        if (_placementPoseIsValid)
        {
            _placementIndicator.SetActive(true);
            _placementIndicator.transform.SetPositionAndRotation(_placementPose.position, _placementPose.rotation);
        }
        else
        {
            _placementIndicator.SetActive(false);
        }
    }

    private void UpdatePlacementPose()  // updates placement pose base on the phone rotation.
    {
        var screenCenter = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        _raycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

        _placementPoseIsValid = hits.Count > 0;
        if (!_placementPoseIsValid) return;
        _placementPose = hits[0].pose;

        var cameraForward = Camera.current.transform.forward;
        var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
        _placementPose.rotation = Quaternion.LookRotation(cameraBearing);
    }

}