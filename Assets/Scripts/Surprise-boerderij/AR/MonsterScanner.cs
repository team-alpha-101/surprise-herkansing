﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//nino

/// <summary>
/// This is a simple script that checks if 1 of the tracked images is similar
/// if the tracked images are similiar it will put the designated monster to activate
/// 
/// This is an unfinished script that only works on 1 monster at the moment but should be converted into working for multiple monsters
/// </summary>

namespace AR
{
    public class MonsterScanner : MonoBehaviour
    {
        private GameObject _origin;
        private GameObject _BasePrefab;
        [SerializeField]
        private GameObject _M1;
        
    
        [SerializeField]
        ARTrackedImageManager m_TrackedImageManager;

        private void Start()
        {    
           _origin = GameObject.FindGameObjectWithTag("origin");
           m_TrackedImageManager = _origin.GetComponent<ARTrackedImageManager>();
            
           m_TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
           
           _BasePrefab = GameObject.FindGameObjectWithTag("base");
           _M1 = _BasePrefab.transform.Find("Monster1").gameObject;
       
           
        
           _M1.SetActive(true);

        }
        
        //TODO: implement scanning multiple targets and searching for the right prefab to activate in the update info function.
        
        

        void OnDisable()
        {
            m_TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
        }

        void UpdateInfo(ARTrackedImage trackedImage)
        {
            if (trackedImage.trackingState != TrackingState.None)
            {
                
               
            }
        }

        void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
        {
            foreach (var trackedImage in eventArgs.added)
            {
                UpdateInfo(trackedImage);
            }

            foreach (var trackedImage in eventArgs.updated)
            {
                UpdateInfo(trackedImage);
            }
           
        }
    }
}
