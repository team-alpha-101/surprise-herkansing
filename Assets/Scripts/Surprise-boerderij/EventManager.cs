﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone

/// <summary>
/// Dit zijn alle events van het project.
/// </summary>

public class EventManager : MonoBehaviour
{
    public static EventManager Instance { get; private set; }

    public delegate void OnResourceUpdate();
    public static event OnResourceUpdate onResourceUpdate;

    public delegate void OnFulfillNeeds(Enums.monsterType monsterType, Enums.resourceType resourceType);
    public static event OnFulfillNeeds onFulfillNeeds;

    public delegate void OnUnlockMonster(Enums.monsterType monsterType);
    public static event OnUnlockMonster onUnlockMonster;

    // Singleton to make sure there is only one of these scripts
    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Event that calls upon the resource update function
    public void ResourceUpdate()
    {
        if (onResourceUpdate != null)
        {
            onResourceUpdate();
        }
    }

    // Event that callls upon The FullfillNeeds function
    public void FulfillNeeds(Enums.monsterType monsterType, Enums.resourceType resourceType)
    {
        if (onFulfillNeeds != null)
        {
            onFulfillNeeds(monsterType, resourceType);
            
        }
    }
    
    // Event that calls upon the UnlockMonster Function
    public void UnlockMonster(Enums.monsterType monsterType)
    {
        if(onUnlockMonster != null)
        {
            onUnlockMonster(monsterType);
        }
    }
}
