﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script takes care of the UI to portray resources
/// </summary>

public class ResourceUI : MonoBehaviour
{
    [SerializeField]
    private Text _waterText;
    [SerializeField]
    private Text _foodText;
    [SerializeField]
    private Text _starpointText;

    // A safety check for the events to make sure there is no memory leak
    private void Awake()
    {
        EventManager.onResourceUpdate += UiUpdate;
    }

    private void OnDestroy()
    {
        EventManager.onResourceUpdate -= UiUpdate;
    }

    // Update the User Interface
    private void UiUpdate()
    {
        _waterText.text = ResourceData.Instance.resources[(int) Enums.resourceType.water].ToString();
        _foodText.text = ResourceData.Instance.resources[(int) Enums.resourceType.food].ToString();
        _starpointText.text = ResourceData.Instance.resources[(int) Enums.resourceType.starpoints].ToString();
    }
}
    
