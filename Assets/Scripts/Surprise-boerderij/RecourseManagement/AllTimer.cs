﻿using UnityEngine;

//sebastiaan

/// <summary>
    /// this script takes care of the resources
    /// </summary>

    public class AllTimer : MonoBehaviour
    {
        [SerializeField]
        private GameObject _button;
        
        private float _timer;

        [SerializeField] private float _timeAmount;



  private void Update()//if statements for activating or disabeling button
  {

      if (_timer >= 0.0f)
      {
          _timer -= Time.deltaTime;
          _button.gameObject.SetActive(false);
      }

      if (_timer <= 0.0f)
      {  
          TimerEnded();
      }
  }
  private void TimerEnded()
  {
      _button.gameObject.SetActive(true);
  }

  public void ResetTimer()//this wil reset the timer with the given time amount
  {
      _timer = _timeAmount;
  }
  
}

