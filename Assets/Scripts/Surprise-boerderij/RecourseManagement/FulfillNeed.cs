﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FulfillNeed : MonoBehaviour
{
    /// <summary>
    /// Tyrone: This sript fulfills the called for need of the designated monster
    /// </summary>
    /// 
    [SerializeField]
    private Enums.monsterType monsterType;
    [SerializeField]
    private Enums.resourceType resourceType;

    private void Start()
    {
        print("start");
    }

    // When clicking on a button it fulfills the specified need for a to be specified monster type and resource
    void OnMouseDown()
    {
        print("MouseDown");
        EventManager.Instance.FulfillNeeds(monsterType, resourceType);
        EventManager.Instance.ResourceUpdate();      
    }
}
