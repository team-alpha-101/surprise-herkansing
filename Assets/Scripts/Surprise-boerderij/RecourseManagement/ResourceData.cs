﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

// Tyrone
/// <summary>
/// This script contains all data for storage and resources
/// </summary>
public class ResourceData : MonoBehaviour
{
    [SerializeField]
    private int[] _resources;

    [SerializeField]
    private int[] _storage;

    [SerializeField]
    private int minimalStorage;

    [SerializeField] 
    private int[] _pointValue ;

    public int[] resources { get { return _resources; } set { _resources = value; } }

    public int[] storage { get { return _storage; } set { _storage = value;}}
    
    public int[] pointValue
    {
        get { return _pointValue;}
        set { _pointValue = value;}
    }


    public static ResourceData Instance;

    // Singleton to make sure there is only one of these active
    private void Awake()
    {
        
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }  
        
        GetData();
    }

    // A check to see if the resources have reached their max and to set it as the max if that is the case
    private void Update()
    {
        for (int i = 0; i < _storage.Length; i++)
        {
            if (_resources[i] > _storage[i])
            {
                _resources[i] = _storage[i];
                EventManager.Instance.ResourceUpdate();
            }
        }
    }

    // Get all the data for resources and storage in playerprefs
    private void GetData()
    {
        
        for (int i = 0; i < _resources.Length; i++)
        {
            _resources[i] = PlayerPrefs.GetInt("Resources" + i);
        }

        for (int i = 0; i < _storage.Length; i++)
        {
            
            _storage[i] = PlayerPrefs.GetInt("Storage" + i);
            if (_storage[i] < minimalStorage)
            {
                _storage[i] = minimalStorage;
            }
        }

        for (int i = 0; i < _pointValue.Length; i++)
        {
            _pointValue[i] = PlayerPrefs.GetInt("PointValue" + i);
            if(_pointValue[i] <= 0){
                _pointValue[i] = 10;
            }
        }
    }

    // Save alll the data for resources and storage in playerprefs
    public void SaveData()
    {
        for (int i = 0; i < _resources.Length; i++)
        {
            PlayerPrefs.SetInt("Resources" + i, _resources[i]);
        }

        for (int i = 0; i < _storage.Length; i++)
        {
            PlayerPrefs.SetInt("Storage" + i, _storage[i]);
        }

        for (int i = 0; i < _pointValue.Length; i++)
        {
            PlayerPrefs.SetInt("PointValue" + i, _pointValue[i]);
        }
    }
}
