﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// with this script you can collect all types of resources
/// </summary>

public class Collect : MonoBehaviour
{
    [SerializeField]
    private Enums.resourceType _resourceType;

    public int _Pointvalue;

   [SerializeField] private ParticleSystem particles;

   [SerializeField] private AllTimer _timer;

   

   void OnMouseDown()//collect resource and add it to the UI
   {
       ResourceData.Instance.resources[(int) _resourceType] += ResourceData.Instance.pointValue[(int) _resourceType];
       ResourceData.Instance.SaveData();
       EventManager.Instance.ResourceUpdate();
       particles.Play();
      _timer.ResetTimer();
   }
}
