﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enum to give the monster types so you can feed different monsters and don't feed them all at once
/// </summary>

public static class Enums
{
    public enum monsterType {greenMonster, blueMonster, redMonster, yellowMonster, orangeMonster, purpleMonster }

    public enum upgradeType {pump = 0, berry = 1, food , water }

    public enum resourceType { water, food, starpoints }
}

