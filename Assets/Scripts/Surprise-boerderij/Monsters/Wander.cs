﻿using UnityEngine;
using System.Collections;

//sebastiaan
/// <summary>
/// This script takes care of the random movmement of the monster that works with navmesh
/// this takes in consideration all obstacles, when the NavMeshAgent moves it will also play an animation
/// </summary>
public class Wander : MonoBehaviour {
 
    public float wanderRadius;
    public float wanderTimer;
      
    private Animator anim;
    private Transform _target;
    private UnityEngine.AI.NavMeshAgent _agent;
    public float _timer;
    [SerializeField]
    private float _animFalse;
 
    void Start(){
        anim = GetComponent<Animator>();//for animations
    }
    void OnEnable () 
    {
        _agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();//assign navmeshagent
        _timer = wanderTimer;
 }
 
    void Update ()      //random walk position and play animation on position change

    {  if(_timer>_animFalse){
        anim.SetBool("walking", false);
     }
         _timer += Time.deltaTime;
        if (!(_timer >= wanderTimer)) return;
        var newPos = RandomNavSphere(transform.position, wanderRadius, -1);
        _agent.SetDestination(newPos);
        _timer = 0; 
        anim.SetBool("walking", true);

    }

    private static Vector3 RandomNavSphere(Vector3 origin, float dist, int layerMask) 
    {//for navmesh movement
        var randDirection = Random.insideUnitSphere * dist;
 
        randDirection += origin;
 
        UnityEngine.AI.NavMeshHit navHit;
 
        UnityEngine.AI.NavMesh.SamplePosition (randDirection, out navHit, dist, layerMask);
 
        return navHit.position;
    }
}