﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone
/// <summary>
/// This scripts makes sure the monster gets hungry and thirsty
/// </summary>

public class Monster : MonoBehaviour
{
    [SerializeField] public GameObject[] _needsButton;

    private float _currentHunger;
    private float _currentThirst;

    [SerializeField]
    private float _maxNeeds = 100;

    [SerializeField]
    private float _needsCheck = 50 ;
    [SerializeField]
    private float _timeCheck = 10;
    [SerializeField]
    private float _needsIncrease = 25;

    public float currentHunger { get { return _currentHunger; } set { _currentHunger = value; } }
    public float currentThirst { get { return _currentThirst; } set { _currentThirst = value; } }
    

    [SerializeField]
    private float _foodTimer;
    [SerializeField]
    private float _waterTimer;

    [SerializeField]
    private float _timeReset;


    // A timer that calls the monster hunger & thirst function if it isn't at the max hunger or thirst already
    public void Update()
    {
        _foodTimer -= Time.deltaTime;
        _waterTimer -= Time.deltaTime;
       
        if (_foodTimer <= _timeCheck && currentHunger < _maxNeeds)
        {
                GainHunger();
                _foodTimer = _timeReset;
        }
        
        if (_waterTimer <= _timeCheck && currentThirst < _maxNeeds)
        {
                GainThirst();
                _waterTimer = _timeReset;
        }
    }

    // Function that gives the monster hunger and activates a button if it is hungry enough
    private void GainHunger()
    {
        print("GainHunger");
        _currentHunger += _needsIncrease;

        if (_currentHunger >= _needsCheck)
        {
            _needsButton[0].SetActive(true);
        }
    }

    // Function that gives the monster hunger and activates a button if it is thirsty enough
    private void GainThirst()
    {
        _currentThirst += _needsIncrease;
        
        if (_currentThirst >= _needsCheck)
        { 
            _needsButton[1].SetActive(true);
        }
    }
}
