﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone
/// <summary>
/// This script contains all the interactions with the monsters
/// </summary>

public class MonsterInteractions : MonoBehaviour
{

    [SerializeField]
    private AudioSource[] _audio;

    [SerializeField]
    private Enums.monsterType _monsterType;

    [SerializeField]
    private ParticleSystem _particles;

    [SerializeField]
    private int _needsDecrease;

    [SerializeField]
    private int _minimalNeed;

    [SerializeField]
    private int _resourceAmount;

    [SerializeField]
    private Monster monster;

    // A safety check for the events to make sure there is no memory leak
    private void Awake()
    {
        EventManager.onFulfillNeeds += FulfillNeeds;
    }

    private void OnDestroy()
    {
        EventManager.onFulfillNeeds -= FulfillNeeds;
    }   

    // Fullfills the need for either water or food for the designated monster
    private void FulfillNeeds(Enums.monsterType monsterType, Enums.resourceType resourceType)
    {
        print("fulfillneeds");  
        if (ResourceData.Instance.resources[(int) resourceType] <= _minimalNeed || monsterType != _monsterType) return;

        ResourceData.Instance.resources[(int) resourceType] -= _resourceAmount;
        _audio[(int) resourceType].Play();

        if (resourceType == Enums.resourceType.food)
        {
            _audio[0].Play();
            monster.currentHunger -= _needsDecrease;
            monster._needsButton[0].SetActive(false);
        }
        else
        {
            _audio[1].Play();
            monster.currentThirst -= _needsDecrease;           
            monster._needsButton[1].SetActive(false);
        }

        GainPoints();
        ResourceData.Instance.SaveData();
        _particles.Play();
    }

    // Adds starpoints to your total when called
    public void GainPoints()
    {
        ResourceData.Instance.resources[(int) Enums.resourceType.starpoints] += _resourceAmount;
    }
}
