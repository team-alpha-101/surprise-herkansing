﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script takes care of unlocking monster in the encylopedia
/// </summary>

public class Encyclopedia : MonoBehaviour
{
    [SerializeField]
    private Canvas _encylopediaCanvas;

    [SerializeField]
    private Image[] _coverImage;

    // Start makes sure all cover images are enabled
    private void Start()
    {
        for (int i = 0; i < _coverImage.Length; i++)
        {
            _coverImage[i].enabled = true;
        }
    }

    // Enabled Eventmanager to be used in this script and makes sure there are no problems by destroying the events if this script stops
    private void Awake()
    {
        EventManager.onUnlockMonster += UnlockMonster;
    }

    private void OnDestroy()
    {
        EventManager.onUnlockMonster -= UnlockMonster;
    }

    // Unlocks the monsters in the encyclopedia if the event calls upon it
    private void UnlockMonster(Enums.monsterType monsterType)
    {
        _coverImage[(int) monsterType].enabled = false;
    }

}
