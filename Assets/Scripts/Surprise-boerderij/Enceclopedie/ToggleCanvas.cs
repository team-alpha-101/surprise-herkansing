﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone
/// <summary>
/// zet de canvas aan of uit
/// </summary>
public class ToggleCanvas : MonoBehaviour
{
    [SerializeField]
    private GameObject _canvas;


    public void Awake()//canvas not visible
    {
        Toggle(false);
    }

    public void OnMouseDown()//canvas visible
    {
        Toggle(true);
    }

    public void Toggle(bool condition)//make it a condition
    {
        _canvas.SetActive(condition);
    }
}
