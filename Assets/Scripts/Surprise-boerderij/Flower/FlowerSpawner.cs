﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;


//nino
/// <summary>
/// dit script spawned bliemen in binnen de navmesh op een timer.
/// </summary>
public class FlowerSpawner : MonoBehaviour
{
    [SerializeField]
    private int _radius;
    [SerializeField]
    private GameObject _prefab;
    [SerializeField]
    private float _timeAmount;
    
    private float TimerFlower;


    private void Start()
    {
        TimerFlower = _timeAmount;
    }

    // Update is called once per frame
    void Update()
    {
        

        if (TimerFlower >= 0.0f)
        {
            TimerFlower -= Time.deltaTime;
        }
 
        if (TimerFlower <= 0.0f)
        {
            Vector3 randomDirection = Random.insideUnitSphere * _radius; 
            randomDirection += transform.position; 
            NavMeshHit hit; 
            NavMesh.SamplePosition(randomDirection, out hit, _radius, 1);

            Vector3 finalPosition = hit.position;
        
            Instantiate(_prefab, finalPosition, Quaternion.identity); 
            
            timerEnded();
        }
     
          
    }
    
    private void timerEnded()
    {
        TimerFlower = _timeAmount;
    }


}
