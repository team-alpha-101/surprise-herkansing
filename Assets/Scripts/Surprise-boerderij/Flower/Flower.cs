﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//nino
/// <summary>
/// dit script houd bij hoelang de bloem nog leeft en zorgt dat in de laatste 10 seconden de bloemv erzameld kan worden.
/// </summary>

public class Flower : MonoBehaviour
{
    [SerializeField]
    private float _timeAmount;
    [SerializeField]
    private GameObject _button;

    private bool _isNotActive = true;
    private float _flowerLifeTime;
    
    void Start()// set button on false and asign timer float
    {
        _flowerLifeTime = _timeAmount;
        _button.gameObject.SetActive(false);
    }

    void Update()//if statements for flower timer to activate or deactivate star button or flower
    {
        
        if (_flowerLifeTime >= 0.0f)
        {
            _flowerLifeTime -= Time.deltaTime;
           
        }
        
        if (_flowerLifeTime <= 10.0f && _flowerLifeTime > 0.0f && _isNotActive)
        {
            _button.gameObject.SetActive(true);
            _isNotActive = false;
        }
 
        if (_flowerLifeTime <= 0.0f)
        {
            timerEnded();
        }
        
    }
    private void timerEnded()//destroy flower
    {
        _flowerLifeTime = _timeAmount;
        Destroy(gameObject);
    }
    
    
}
