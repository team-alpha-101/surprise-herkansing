﻿using UnityEngine;
 using UnityEngine.UI;
 using UnityEngine.Sprites;
 using System.Collections;
//sebastiaan
/// <summary>
/// dits script regelt alle slides van de tutorial
/// je kan hier simpel met buttons doorheen gaan.
/// </summary>

public class TutorialController : MonoBehaviour {
 
 [SerializeField]
 private Sprite[] _gallery;
 [SerializeField]
 private Image _displayImage; 
 [SerializeField]
 private Button _nextImg; 
 [SerializeField]
 private Button _prevImg; 
 private int i = 0; //Will control where in the array you are
 [SerializeField]
 private GameObject _buttonClose;
 [SerializeField]
 private GameObject _tutorial;
 [SerializeField]
 private  PrefTutorial _preft;
  [SerializeField]
 private  GameObject _ActivateTutButton;
 [SerializeField]
 private int _amountImages;


 //next and previous for the image controller
   public void BtnNext (){
          if(i + 1 < _gallery.Length){
              i++;
          }
      }
     
   public void BtnPrev () {
          if (i > 0){
              i --;
          }
      }
 
   private void Update () {//if you reached the end of the galery, setactive to true
    _displayImage.sprite = _gallery[i];
        if(i==_amountImages){
    _buttonClose.SetActive(true);
   
    
    }
  }
  public void DisableTutorial(){//disable the tutorial if close has been clicked
      _tutorial.SetActive(false);
      _preft.tutiefrutie=true;
      _ActivateTutButton.SetActive(true);
  }
   public void Reset(){// reset the tutorial
      _tutorial.SetActive(true);
      _preft.tutiefrutie=false;
      _ActivateTutButton.SetActive(false);
  }
 }
 