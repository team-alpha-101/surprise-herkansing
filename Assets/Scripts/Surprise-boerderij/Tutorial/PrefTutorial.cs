﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//sebastiaan
/// <summary>
/// this script checks if you have already seen the tutorial
/// </summary>

public class PrefTutorial : MonoBehaviour
{
    [SerializeField]
    private bool _tutorialSeen;
    [SerializeField]
    private GameObject _tutorial;
    
    public bool tutiefrutie { get { return _tutorialSeen; } set { _tutorialSeen = value; } }
    
    
    void Update(){

    if(_tutorialSeen==true){
        _tutorial.SetActive(false);
          }
    }

   
} 