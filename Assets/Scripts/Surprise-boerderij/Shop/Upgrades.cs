﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

// Tyrone
/// <summary>
/// All upgrades for available in the shop
/// </summary>

public class Upgrades : MonoBehaviour
{
    [SerializeField]
    private Image[] _upgradeIcons;

    [SerializeField]
    private Button[] _upgradeButtons;

    [SerializeField]
    private Text[] _costText;

    [SerializeField]
    private int _maxStorageIncrease = 50;

    [SerializeField] 
    private int _resourceIncrease = 10;

    private int _arrayOffset = 2;


    // Set up starting cost for each upgrade and portray it in game, also checks if the upgrades are available at start
    private void Start()
    {
        
        CostData.Instance.GetCostData();
        EventManager.Instance.ResourceUpdate();

        for (int i = 0; i < CostData.Instance._costs.Length; i++)
        {
            if (CostData.Instance._costs[i] <= CostData.Instance.startCost)
            {
                CostData.Instance._costs[i] = CostData.Instance.startCost;
            }

            _costText[i].text = CostData.Instance._costs[i].ToString();
        }
    }

    // UpgradeChecks Checks if the upgrade is upgradable or already reached the max level of the upgrade and calls upon the upgrade function if possible

    private void UpgradeCheck(Enums.upgradeType upgradetype)
    {
        if (CostData.Instance._costs[(int) upgradetype] >= CostData.Instance.maxLvlCost)
        {
            _upgradeButtons[(int)upgradetype].interactable = false;
            _costText[(int)upgradetype].text = "Max";
            _upgradeIcons[(int)upgradetype].enabled = false;
        }
        else
        {       
            Upgrade(upgradetype);
        }
    }

    // Upgrades the max storage of Resources or the amount of resources you get when called upon
    private void Upgrade(Enums.upgradeType upgradetype)
    {
        if (ResourceData.Instance.resources[(int) Enums.resourceType.starpoints] >= CostData.Instance._costs[(int)upgradetype])
        {
            ResourceData.Instance.resources[(int) Enums.resourceType.starpoints] -= CostData.Instance._costs[(int)upgradetype];
            CostData.Instance._costs[(int)upgradetype] += CostData.Instance.costIncrease;
            
            switch (upgradetype)
            {
                case Enums.upgradeType.pump:
                    ResourceData.Instance.storage[(int)Enums.upgradeType.pump] += _maxStorageIncrease;
                    break;
                
                case Enums.upgradeType.berry:
                    ResourceData.Instance.storage[(int)Enums.upgradeType.berry] += _maxStorageIncrease;
                    break;
                
                case Enums.upgradeType.water:
                    ResourceData.Instance.pointValue[(int)Enums.upgradeType.water - _arrayOffset] += _resourceIncrease;
                    break;
                
                case Enums.upgradeType.food:
                    ResourceData.Instance.pointValue[(int)Enums.upgradeType.food - _arrayOffset] += _resourceIncrease;
                    break;
            }
            
            ResourceData.Instance.SaveData();
            EventManager.Instance.ResourceUpdate();
            CostData.Instance.SaveCostData();
            _costText[(int)upgradetype].text = CostData.Instance._costs[(int)upgradetype].ToString();
        }
    }

    // calls the upgradecheck and gives it a type to upgrade
    public void PumpUpgrade()
    {
        UpgradeCheck(Enums.upgradeType.pump);
    }
    public void BerryUpgrade()
    {
        UpgradeCheck(Enums.upgradeType.berry);
    }
    public void WaterUpgrade()
    {
        UpgradeCheck(Enums.upgradeType.water);
    }
    public void FoodUpgrade()
    {
        UpgradeCheck(Enums.upgradeType.food);
    }
}
