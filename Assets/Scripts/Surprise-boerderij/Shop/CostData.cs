﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script saves all cost data for the shop
/// </summary>

public class CostData : MonoBehaviour
{
    public static CostData Instance;

    public int[] _costs;

    [SerializeField]
    private int _maxLvlCost = 500;

    [SerializeField]
    private int _startCost = 100;

    [SerializeField]
    private int _costIncrease = 100;

    public int maxLvlCost { get { return _maxLvlCost; } set { _maxLvlCost = value; } }
    public int startCost { get { return _startCost; } set { _startCost = value; } }

    public int costIncrease { get { return _costIncrease; } set { _costIncrease = value; } }

    // A singleton to make sure there is only one of these active
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        
        GetCostData();
    }

    // Gets cost data from playerPref
    public void GetCostData()
    {
        for (int i = 0; i < _costs.Length; i++)
        {
            _costs[i] = PlayerPrefs.GetInt("Cost" + i + 1);
        }
    }

    // Saves cost data in playerprefs
    public void SaveCostData()
    {
        for (int i = 0; i < _costs.Length; i++)
        {
            PlayerPrefs.SetInt("Cost" + i + 1, _costs[i]);
        }
    }
}
